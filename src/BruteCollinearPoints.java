import java.util.ArrayList;
import java.util.Arrays;


/**
 * A BruteForce solution for finding collinear points in a set of points.
 *
 * @author Bogdan Rak
 */
public class BruteCollinearPoints {

    private ArrayList<LineSegment> segments;

    public BruteCollinearPoints(Point[] points) {
        checkDuplicatedEntries(points);
        segments = new ArrayList<LineSegment>();
        Point[] sortedPoints = Arrays.copyOf(points, points.length);
        Arrays.sort(sortedPoints);
        for (int p = 0, l = sortedPoints.length; p < l - 3; p++) {
            for (int q = p + 1; q < l - 2; q++) {
                for (int r = q + 1; r < l - 1; r++) {
                    for (int s = r + 1; s < l; s++) {
                        System.out.println("" + p + " " + q + " " + r + " " + s);
                        if (sortedPoints[p].slopeTo(sortedPoints[q]) == sortedPoints[p].slopeTo(sortedPoints[r])
                            && sortedPoints[p].slopeTo(sortedPoints[r]) == sortedPoints[p].slopeTo(sortedPoints[s])) {
                            segments.add(new LineSegment(sortedPoints[p], sortedPoints[s]));
                        }
                    }
                }
            }
        }
    }

    public int numberOfSegments() {
        return segments.size();
    }

    public LineSegment[] segments() {
        return this.segments.toArray(new LineSegment[numberOfSegments()]);
    }

    private void checkDuplicatedEntries(Point[] points) {
        for (int i = 0, j = 0, l = points.length; i < l; ++i) {
            for (j = i + 1; j < l; ++j) {
                if (0 == points[i].compareTo(points[j])) {
                    throw new IllegalArgumentException("Duplicated entries in given points.");
                }
            }
        }
    }

    public static void main(String[] args) {
        Point []points = new Point[8];
        points[0] = new Point(0, 1);
        points[1] = new Point(1, 2);
        points[2] = new Point(2, 3);
        points[3] = new Point(3, 4);
        points[4] = new Point(0, 2);
        points[5] = new Point(2, 4);
        points[6] = new Point(4, 6);
        points[7] = new Point(6, 8);
        BruteCollinearPoints bc = new BruteCollinearPoints(points);
        System.out.println("COUNT = " + bc.numberOfSegments());
    }
}